# Python

此作业为Python大作业

## 设计思路

我们可以用Cookie将我们的请求头伪装成用户，进行对网易云用户数据的访问，同时使用网易云官方api来实现用书数据的获取，再用数据清洗来实现数据的分类，再使用jieba2进行文本分析，之后使用数据库进行数据存储，清洗完成后使用可视化框架来展现出来、。



### 难点1

因为不理解网易云反扒取机制导致我们前期即使用cookie进行伪装之后也无法获取数据最后借鉴了GitHub大佬spider_collection提供的项目进行伪装最后成功



### 难点2

为了使访问api稳定的实时爬虫的稳定性同时减少了用户配置爬虫的阶段，我们将api部署到了云服务器Serverless进行处理



### 难点3

![image-20231123214342680](C:\Users\mashiro\AppData\Roaming\Typora\typora-user-images\image-20231123214342680.png)

echarts框架得到数据不渲染



### 难点4

可视化输出读取不到redis数据库的数据，（可能是其他项目占用了redis数据库导致占用了未知端口导致jdbc连接失败）

最后根据教程使用csv存储数据[python - csv 文件读取、处理、写入_python csv-CSDN博客](https://blog.csdn.net/weixin_46124984/article/details/124655560)



### 难点5

各个功能的实现







## 所查资料

1.本人的哔哩哔哩账号里面的学习[mashiro_kk的个人空间-mashiro_kk个人主页-哔哩哔哩视频 (bilibili.com)](https://space.bilibili.com/325914817?spm_id_from=333.1007.0.0)

2.参考了这个文档解决数据库存储[python - csv 文件读取、处理、写入_python csv-CSDN博客](https://blog.csdn.net/weixin_46124984/article/details/124655560)

3.网易云官方api文档(https://github.com/Binaryify/NeteaseCloudMusicApi)

4.部署云服务器使用的官方文档https://console.cloud.tencent.com/sls

5https://raw.githubusercontent.com/Binaryify/NeteaseCloudMusicApi/master/static/docs.png

6.[基础python+Flask+爬虫 ](https://www.bilibili.com/video/BV12E411A7ZQ?from=search&seid=17327553224685529336) 通过这个课程了解了爬虫的基础知识并且学习了网站搭建的知识（负基础教程，老师讲的很细很细，我是跳着看的，还是很推荐的

7.HTML： [站长之家](https://aspx.sc.chinaz.com/) 本项目使用了来自该网站的网页模板，均为免费资源。本项目在实际设计中对原样板网站做了大量删改，除框架外基本原创。

8.Echarts： [Echarts](https://echarts.apache.org/zh/index.html) 本项目内的可视化方案大部分由echarts提供，项目中引用了echarts.js和bmap.js两个JS文件

### 收获体会

这个项目作为学习python的一个项目来说工程量较为大，很适合作为初学者来学习既可以熟练使用框架也可以让自己工程能力和找到办法解决办法的能力提升。最后将学习做的项目开源出来，后续会继续补足功能写完Readme文档的。